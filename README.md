# HOOKS #

Git hooks are scripts that run automatically each time you perform a particular action on the Git repository. They allow you to modify Git's internal behavior.

### How it works ###

Every Git repository, Bitbucket, among others, when created, generates a .git folder, which is usually hidden by operating systems. Inside this folder all the magic of versioning, logs, branch separation and so on is done. And there we also have the hooks, if you list what is in the .git folder, you will see something like:

* branches
* hooks
* info
* logs
* objects
* refs

As we will work with Hooks, we will use the hooks folder. If we list the files, we will have:

* applypatch-msg.sample
* commit-msg.sample
* post-update.sample
* pre-applypatch.sample
* pre-commit.sample
* pre-push.sample
* pre-rebase.sample
* prepare-commit-msg.sample
* update.sample

If you notice, they already have very different names when they will be activated. Everyone has the extension .sample and to make them work just remove that extension. They all already have some code inside, for example if I get the commit-msg.sample file.

### To begin ###

* First run the command:

$ make hooks

This command will install Lint and then create a physical link between the scripts / pre-commit.sh and .git / hooks / pre-commit file to define pre-commit commands.